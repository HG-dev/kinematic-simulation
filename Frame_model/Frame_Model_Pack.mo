package Frame_Model_Pack
model Frame_Model
  import Modelica.SIunits.Conversions.*;
  import Modelica.Constants.*;
  parameter Modelica.SIunits.Length initial_drop = 0 "Distance below origin keel initiates at";
  parameter Modelica.SIunits.ModulusOfElasticity E_steel = 200e9 "Young's modulus of steel (for wires)";
  parameter Modelica.SIunits.Length wire_dia = 2.5e-3 "Diameter of wires";
  parameter Modelica.SIunits.TranslationalSpringConstant wire_stiffness = 0.9*E_steel * pi * (wire_dia / 2) ^ 2 "spring constant for wires";
  parameter Modelica.SIunits.Length noseWireLength = 2.149 "Nose wire lengths";
  parameter Modelica.SIunits.Length rearWireLength = 1.888 "Rear wire lengths";
  parameter Modelica.SIunits.Length sideWireLength = 2.743 "Side wire lengths";
  parameter Modelica.SIunits.Length control_frame_height = 1.5665 "Height of control frame";
  parameter Modelica.SIunits.Angle noseAngle = from_deg(127.4) "Nose angle of glider";
  parameter Modelica.SIunits.Length NPx = 0.05795 "Set back of leading edge hinge along keel";
  parameter Modelica.SIunits.Length NPy = 0.0635 "Distance outboard of leading edge hinge from keel";
  parameter Modelica.SIunits.Length keelLength = 2.28 "Overall length of keel";
  parameter Modelica.SIunits.Length TS_dist = 2.14 "Distance from nose to tension strop/rear wire attachment point";
  parameter Modelica.SIunits.Length TOU_dist = 1.45 "Distance from nose to top of upright attachment point";
  parameter Modelica.SIunits.Length TE_dist = 1.855 "Distance from nose to trailing edge of sail";
  parameter Modelica.SIunits.Length hang_point_dist = 1.325 "Distance from nose to hang point";
  parameter Modelica.SIunits.Length xtube_dist = 0.731 "Distance from nose to xtube crossing keel";
  parameter Modelica.SIunits.Length TSLength = sqrt((TS_dist-lev_length_TS-xtube_dist)^2 + lev_height^2) "Distance from nose to hang point";
  parameter Modelica.SIunits.Length LEI_length = 4.104 "Length of inner leading edge from bolt to end of tube";
  parameter Modelica.SIunits.Length LEI_sidewire_dist = 3.234 "Distance from bolt to sidewire fixing";
  parameter Modelica.SIunits.Length LEI_xtube_x = 2.834 "Distance from bolt to cross tube junction along tube";
  parameter Modelica.SIunits.Length LEI_xtube_y = 0.03 "Distance to cross tube junction across tube";
  parameter Modelica.SIunits.Length LEI_xtube_z = 0.057 "Distance to cross tube junction above tube";
  parameter Modelica.SIunits.Length susp_point = LEI_xtube_x "Distance to supsension point along leading edge";
  parameter Modelica.SIunits.Length LEO_length = 1.172 "Distance LEO protrudes from LEI";
  parameter Modelica.SIunits.Length xtube_length = 2.615 "Length of cross-tube (joint to joint)";
  parameter Modelica.SIunits.Length lev_plate_width = 0.097 "Width of leveller plates (hole to hole)";
  parameter Modelica.SIunits.Length lev_height = 0.1 "Height of leveller";
  parameter Modelica.SIunits.Length lev_length = 0.364 "Length of leveller";
  parameter Modelica.SIunits.Length lev_length_TS = 0.1 "Length of leveller to tension strop attachment";
  parameter Modelica.SIunits.Angle fudgeAngle = from_deg(0.1) "Annoying fudge angle to make things not quite line up!";
  parameter Modelica.SIunits.Angle xtubeAngle = acos((NPy - lev_plate_width / 2 + sin(noseAngle / 2) * LEI_xtube_x - sin(from_deg(90) - noseAngle / 2) * LEI_xtube_y) / xtube_length) + fudgeAngle "Angle of cross tube relative to y axis";
  parameter Modelica.SIunits.Angle xtubeLEIAngle_R = from_deg(270) - noseAngle / 2 - xtubeAngle "Angle of cross tube relative to leading edge";
  parameter Modelica.SIunits.Angle xtubeLEIAngle_L = from_deg(90) + noseAngle / 2 + xtubeAngle "Angle of cross tube relative to leading edge";
  parameter Modelica.SIunits.Length slack_length = 4.93 "Laid flat slack length of the sail on each side";
  
  Frame_Model_Pack.Control_Frame control_Frame(height = control_frame_height) annotation(
    Placement(visible = true, transformation(origin = {-12, -40}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Joints.Revolute TOU_rev(n = {0, 1, 0}, phi(fixed = true, start = 0.0862891), w(fixed = true, start = 0)) annotation(
    Placement(visible = true, transformation(origin = {-22, -10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Frame_Model_Pack.Leading_Edge_Inner LEI_R(length = LEI_length, sidewire_dist = LEI_sidewire_dist, susp_point = susp_point, xtube_x = LEI_xtube_x, xtube_y = -LEI_xtube_y, xtube_z = LEI_xtube_z) annotation(
    Placement(visible = true, transformation(origin = {-98, 106}, extent = {{-50, -10}, {50, 10}}, rotation = 90)));
  Frame_Model_Pack.Leading_Edge_Inner LEI_L(length = LEI_length, sidewire_dist = LEI_sidewire_dist, susp_point = susp_point, xtube_x = LEI_xtube_x, xtube_y = LEI_xtube_y, xtube_z = LEI_xtube_z) annotation(
    Placement(visible = true, transformation(origin = {-100, -98}, extent = {{-50, -10}, {50, 10}}, rotation = 270)));
  Modelica.Mechanics.MultiBody.Parts.BodyCylinder xtube_R(density = 2400, diameter = 0.062, innerDiameter = 0.06, r = {xtube_length, 0, 0}) annotation(
    Placement(visible = true, transformation(origin = {-43, 115}, extent = {{-17, -17}, {17, 17}}, rotation = -90)));
  Modelica.Mechanics.MultiBody.Joints.Revolute xtube_junc_R(phi(fixed = true, start = xtubeLEIAngle_R), w(fixed = true)) annotation(
    Placement(visible = true, transformation(origin = {-74, 134}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Parts.BodyCylinder xtube_L(density = 2400, diameter = 0.062, innerDiameter = 0.06, r = {xtube_length, 0, 0}) annotation(
    Placement(visible = true, transformation(origin = {-55, -81}, extent = {{-17, -17}, {17, 17}}, rotation = 90)));
  Modelica.Mechanics.MultiBody.Joints.Revolute xtube_junc_L(phi(fixed = true, start = xtubeLEIAngle_L), w(fixed = true)) annotation(
    Placement(visible = true, transformation(origin = {-76, -126}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Forces.SpringDamperParallel lev_fudge_joint(animation = false,c = 10000000, d = 10000000, s_unstretched = 0) annotation(
    Placement(visible = true, transformation(origin = {-46, -36}, extent = {{-6, -6}, {6, 6}}, rotation = 90)));
  Modelica.Mechanics.MultiBody.Forces.SpringDamperParallel nosewire_L(c = wire_stiffness / noseWireLength, coilWidth = 0, d = wire_stiffness, diameter_a = 0.0025, diameter_b = 0.0025, numberOfWindings = 0, s_unstretched = noseWireLength, width = 0.001) annotation(
    Placement(visible = true, transformation(origin = {-74, -26}, extent = {{-6, -6}, {6, 6}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Forces.SpringDamperParallel nosewire_R(c = wire_stiffness / noseWireLength, coilWidth = 0, d = wire_stiffness, diameter_a = 0.0025, diameter_b = 0.0025, numberOfWindings = 0, s_unstretched = noseWireLength, width = 0.001) annotation(
    Placement(visible = true, transformation(origin = {-74, -6}, extent = {{-6, -6}, {6, 6}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Forces.SpringDamperParallel rearwire_L(c = wire_stiffness / rearWireLength, coilWidth = 0, d = wire_stiffness, diameter_a = 0.0025, diameter_b = 0.0025, numberOfWindings = 0, s_unstretched = rearWireLength, width = 0.001) annotation(
    Placement(visible = true, transformation(origin = {56, -24}, extent = {{-6, -6}, {6, 6}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Forces.SpringDamperParallel rearwire_R(c = wire_stiffness / rearWireLength, coilWidth = 0, d = wire_stiffness, diameter_a = 0.0025, diameter_b = 0.0025, numberOfWindings = 0, s_unstretched = rearWireLength, width = 0.001) annotation(
    Placement(visible = true, transformation(origin = {44, -54}, extent = {{-6, -6}, {6, 6}}, rotation = 0)));
Modelica.Mechanics.MultiBody.Interfaces.Frame_a nose_point annotation(
    Placement(visible = true, transformation(origin = {-160, 10}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {-78, 0}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
Modelica.Mechanics.MultiBody.Interfaces.Frame_b susp_point_R annotation(
    Placement(visible = true, transformation(origin = {-158, 128}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {-10, 146}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
Modelica.Mechanics.MultiBody.Interfaces.Frame_b susp_point_L annotation(
    Placement(visible = true, transformation(origin = {-170, -126}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {-10, -136}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
Modelica.Mechanics.MultiBody.Interfaces.Frame_b hang_point annotation(
    Placement(visible = true, transformation(origin = {-66, 48}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {20, 2}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
Modelica.Mechanics.MultiBody.Parts.FixedRotation tip_R(angle = (-180) + to_deg(noseAngle / 2), n = {0, 0, 1}, r = {0.47, 0, 0}, width = 0.02) annotation(
    Placement(visible = true, transformation(origin = {-52, 188}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
Modelica.Mechanics.MultiBody.Parts.FixedRotation fixedRotation(angle = 180 - to_deg(noseAngle / 2), n = {0, 0, 1}, r = {0.47, 0, 0}, width = 0.02) annotation(
    Placement(visible = true, transformation(origin = {-44, -186}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
Modelica.Mechanics.MultiBody.Interfaces.Frame_b control_point annotation(
    Placement(visible = true, transformation(origin = {88, -72}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {-6, -38}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
Modelica.Mechanics.MultiBody.Forces.SpringDamperParallel springDamperParallel(animation = false, c = 10000000, d = 10000000, s_unstretched = 0) annotation(
    Placement(visible = true, transformation(origin = {-42, 76}, extent = {{-6, -6}, {6, 6}}, rotation = 90)));
Frame_Model_Pack.Keel_Assem keel_Assem(NPx = NPx, NPy = NPy,TE_dist = TE_dist, TOU_dist = TOU_dist, TSLength = TSLength, TS_dist = TS_dist, TS_stiff = 1e5, hang_point_dist = hang_point_dist, keelLength = keelLength, lev_height = LEI_xtube_z, lev_length = lev_length, lev_length_TS = lev_length_TS)  annotation(
    Placement(visible = true, transformation(origin = {-34, 9.6}, extent = {{-68, -13.6}, {68, 13.6}}, rotation = 0)));
Modelica.Mechanics.MultiBody.Sensors.Distance sail_dist_L(animation = false)  annotation(
    Placement(visible = true, transformation(origin = {78, -132}, extent = {{10, -10}, {-10, 10}}, rotation = 90)));
Modelica.Mechanics.MultiBody.Sensors.Distance sail_dist_R(animation = false)  annotation(
    Placement(visible = true, transformation(origin = {88, 106}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
Modelica.Mechanics.MultiBody.Joints.Revolute nose_hinge_R(n = {0, 0, 1}, phi(fixed = true, start = noseAngle / 2), w(fixed = true, start = 0))  annotation(
    Placement(visible = true, transformation(origin = {-98, 40}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
Modelica.Mechanics.MultiBody.Joints.Revolute nose_hinge_L(n = {0, 0, 1}, phi(fixed = true, start = -noseAngle / 2), w(fixed = true, start = 0)) annotation(
    Placement(visible = true, transformation(origin = {-98, -26}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
Modelica.Blocks.Math.Add keel_shift(k2 = -1)  annotation(
    Placement(visible = true, transformation(origin = {134, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
Sail_Tension sail_tension_R(slack_length = slack_length)  annotation(
    Placement(visible = true, transformation(origin = {56, 106}, extent = {{-50, -10}, {50, 10}}, rotation = 90)));
Sail_Tension sail_tension_L(slack_length = slack_length)  annotation(
    Placement(visible = true, transformation(origin = {48, -134}, extent = {{50, -10}, {-50, 10}}, rotation = 90)));
Modelica.Mechanics.MultiBody.Sensors.CutForce weight_R(animation = false)  annotation(
    Placement(visible = true, transformation(origin = {-126, 128}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
Modelica.Mechanics.MultiBody.Sensors.CutForce weight_L(animation = false)  annotation(
    Placement(visible = true, transformation(origin = {-130, -124}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
Frame_Model_Pack.Leading_Edge_Outer LEO_L(length = LEO_length)  annotation(
    Placement(visible = true, transformation(origin = {-101, -184.2}, extent = {{-31, -6.2}, {31, 6.2}}, rotation = -90)));
Frame_Model_Pack.Leading_Edge_Outer LEO_R(length = LEO_length)  annotation(
    Placement(visible = true, transformation(origin = {-97, 195.8}, extent = {{-29, -5.8}, {29, 5.8}}, rotation = 90)));
Modelica.Mechanics.MultiBody.Parts.FixedTranslation fixedTranslation(r = {LEI_length + LEO_length, 0, 0}, width = 0.02)  annotation(
    Placement(visible = true, transformation(origin = {-114, 174}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
Modelica.Mechanics.MultiBody.Sensors.Distance distance annotation(
    Placement(visible = true, transformation(origin = {-126, 258}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Forces.SpringDamperParallel siderwire_R(c = wire_stiffness / sideWireLength, coilWidth = 0, d = wire_stiffness, diameter_a = 0.0025, diameter_b = 0.0025, numberOfWindings = 0, s_unstretched = sideWireLength, width = 0.001) annotation(
      Placement(visible = true, transformation(origin = {24, 66}, extent = {{-6, -6}, {6, 6}}, rotation = 90)));
  Modelica.Mechanics.MultiBody.Forces.SpringDamperParallel sidewire_L(c = wire_stiffness / sideWireLength, coilWidth = 0, d = wire_stiffness, diameter_a = 0.0025, diameter_b = 0.0025, numberOfWindings = 0, s_unstretched = sideWireLength, width = 0.001) annotation(
      Placement(visible = true, transformation(origin = {-4, -96}, extent = {{-6, -6}, {6, 6}}, rotation = 90)));
  Modelica.Blocks.Math.Add average(k1 = +0.16, k2 = +0.16) annotation(
      Placement(visible = true, transformation(origin = {-126, -50}, extent = {{-12, -12}, {12, 12}}, rotation = 0)));
  equation
    connect(TOU_rev.frame_b, control_Frame.TOU_conn) annotation(
      Line(points = {{-12, -10}, {-12, -20}}, color = {95, 95, 95}));
    connect(LEI_R.xtube_junc, xtube_junc_R.frame_a) annotation(
      Line(points = {{-98, 134}, {-84, 134}, {-84, 134}, {-84, 134}}));
    connect(xtube_junc_R.frame_b, xtube_R.frame_a) annotation(
      Line(points = {{-64, 134}, {-43, 134}, {-43, 132}}));
    connect(xtube_junc_L.frame_b, xtube_L.frame_a) annotation(
      Line(points = {{-66, -126}, {-54, -126}, {-54, -98}, {-54, -98}}, color = {95, 95, 95}));
    connect(LEI_L.xtube_junc, xtube_junc_L.frame_a) annotation(
      Line(points = {{-100, -126}, {-86, -126}}));
    connect(xtube_L.frame_b, lev_fudge_joint.frame_a) annotation(
      Line(points = {{-54, -64}, {-54, -46}, {-46, -46}, {-46, -42}}, color = {95, 95, 95}));
    connect(nosewire_L.frame_b, control_Frame.BOU_corner_L) annotation(
      Line(points = {{-68, -26}, {-60, -26}, {-60, -52}, {-32, -52}, {-32, -52}}, color = {95, 95, 95}));
    connect(nosewire_R.frame_b, control_Frame.BOU_corner_R) annotation(
      Line(points = {{-68, -6}, {-62, -6}, {-62, -60}, {12, -60}, {12, -52}, {8, -52}, {8, -52}}, color = {95, 95, 95}));
    connect(rearwire_L.frame_a, control_Frame.BOU_corner_L) annotation(
      Line(points = {{50, -24}, {10, -24}, {10, -56}, {-32, -56}, {-32, -52}}, color = {95, 95, 95}));
    connect(rearwire_R.frame_a, control_Frame.BOU_corner_R) annotation(
      Line(points = {{38, -54}, {23, -54}, {23, -52}, {8, -52}}));
    connect(control_Frame.BOU_corner_L, control_point) annotation(
      Line(points = {{-32, -52}, {-32, -72}, {88, -72}}, color = {95, 95, 95}));
    connect(xtube_R.frame_b, springDamperParallel.frame_b) annotation(
      Line(points = {{-42, 98}, {-42, 98}, {-42, 82}, {-42, 82}}, color = {95, 95, 95}));
    connect(nose_point, keel_Assem.nose_conn) annotation(
      Line(points = {{-160, 10}, {-101, 10}, {-101, 9}}));
    connect(keel_Assem.hang_point_conn, hang_point) annotation(
      Line(points = {{-26, 9}, {-26, 46}, {-66, 46}, {-66, 48}}, color = {95, 95, 95}));
    connect(keel_Assem.TOU_anchor_conn, TOU_rev.frame_a) annotation(
      Line(points = {{-17, 9}, {-20, 9}, {-20, 2}, {-40, 2}, {-40, -10}, {-32, -10}}, color = {95, 95, 95}));
    connect(lev_fudge_joint.frame_b, keel_Assem.lev_L_conn) annotation(
      Line(points = {{-46, -30}, {-46, -16}, {-56, -16}, {-56, -2}}, color = {95, 95, 95}));
    connect(keel_Assem.lev_R_conn, springDamperParallel.frame_a) annotation(
      Line(points = {{-56, 19}, {-56, 45.5}, {-42, 45.5}, {-42, 70}}, color = {95, 95, 95}));
    connect(keel_Assem.TS_anchor_conn, rearwire_L.frame_b) annotation(
      Line(points = {{26, 10}, {26, 10}, {26, -10}, {72, -10}, {72, -24}, {62, -24}, {62, -24}}, color = {95, 95, 95}));
    connect(rearwire_R.frame_b, keel_Assem.TS_anchor_conn) annotation(
      Line(points = {{50, -54}, {72, -54}, {72, -8}, {26, -8}, {26, 10}, {26, 10}}, color = {95, 95, 95}));
    connect(nosewire_L.frame_a, keel_Assem.nose_conn) annotation(
      Line(points = {{-80, -26}, {-114, -26}, {-114, 10}, {-102, 10}, {-102, 10}}, color = {95, 95, 95}));
    connect(nosewire_R.frame_a, keel_Assem.nose_conn) annotation(
      Line(points = {{-80, -6}, {-114, -6}, {-114, 10}, {-102, 10}, {-102, 10}}));
    connect(nose_hinge_R.frame_b, LEI_R.LE_bolt) annotation(
      Line(points = {{-98, 50}, {-98, 50}, {-98, 56}, {-98, 56}}, color = {95, 95, 95}));
    connect(nose_hinge_R.frame_a, keel_Assem.NP_R_conn) annotation(
      Line(points = {{-98, 30}, {-98, 30}, {-98, 26}, {-94, 26}, {-94, 22}, {-92, 22}}));
    connect(nose_hinge_L.frame_b, LEI_L.LE_bolt) annotation(
      Line(points = {{-98, -36}, {-100, -36}, {-100, -48}, {-100, -48}}));
    connect(nose_hinge_L.frame_a, keel_Assem.NP_L_conn) annotation(
      Line(points = {{-98, -16}, {-98, -16}, {-98, -4}, {-92, -4}, {-92, -4}}, color = {95, 95, 95}));
    connect(sail_dist_L.distance, keel_shift.u2) annotation(
      Line(points = {{89, -132}, {104, -132}, {104, -6}, {122, -6}}, color = {0, 0, 127}));
    connect(sail_dist_R.distance, keel_shift.u1) annotation(
      Line(points = {{99, 106}, {104, 106}, {104, 6}, {122, 6}}, color = {0, 0, 127}));
    connect(sail_tension_R.frame_a, keel_Assem.sail_anchor_conn) annotation(
      Line(points = {{56, 96}, {56, 96}, {56, 32}, {16, 32}, {16, 10}, {18, 10}}));
    connect(sail_tension_L.frame_a, keel_Assem.sail_anchor_conn) annotation(
      Line(points = {{48, -124}, {48, -16}, {16, -16}, {16, 10}, {18, 10}}));
    connect(sail_tension_L.frame_b, sail_dist_L.frame_b) annotation(
      Line(points = {{48, -144}, {78, -144}, {78, -142}, {78, -142}}, color = {95, 95, 95}));
    connect(sail_tension_L.frame_a, sail_dist_L.frame_a) annotation(
      Line(points = {{48, -124}, {78, -124}, {78, -122}, {78, -122}}));
    connect(sail_tension_R.frame_a, sail_dist_R.frame_a) annotation(
      Line(points = {{56, 96}, {88, 96}, {88, 96}, {88, 96}}));
    connect(sail_tension_R.frame_b, sail_dist_R.frame_b) annotation(
      Line(points = {{56, 116}, {86, 116}, {86, 116}, {88, 116}}));
    connect(susp_point_R, weight_R.frame_a) annotation(
      Line(points = {{-158, 128}, {-136, 128}}));
    connect(weight_R.frame_b, LEI_R.supension_point) annotation(
      Line(points = {{-116, 128}, {-107, 128}, {-107, 130}, {-98, 130}}, color = {95, 95, 95}));
    connect(susp_point_L, weight_L.frame_a) annotation(
      Line(points = {{-170, -126}, {-140, -126}, {-140, -124}, {-140, -124}}));
    connect(LEI_L.supension_point, weight_L.frame_b) annotation(
      Line(points = {{-100, -122}, {-120, -122}, {-120, -124}, {-120, -124}}, color = {95, 95, 95}));
    connect(LEI_L.LEI_end, LEO_L.LEO_Inner) annotation(
      Line(points = {{-100, -148}, {-100, -148}, {-100, -154}, {-100, -154}}, color = {95, 95, 95}));
    connect(LEO_L.LEO_end, fixedRotation.frame_b) annotation(
      Line(points = {{-100, -214}, {-100, -214}, {-100, -220}, {-78, -220}, {-78, -184}, {-54, -184}, {-54, -186}}));
    connect(LEO_R.LEO_Inner, LEI_R.LEI_end) annotation(
      Line(points = {{-96, 168}, {-98, 168}, {-98, 156}, {-98, 156}}));
    connect(LEO_R.LEO_end, tip_R.frame_b) annotation(
      Line(points = {{-96, 224}, {-98, 224}, {-98, 232}, {-78, 232}, {-78, 188}, {-62, 188}, {-62, 188}}, color = {95, 95, 95}));
    connect(fixedTranslation.frame_a, LEI_R.LE_bolt) annotation(
      Line(points = {{-114, 164}, {-112, 164}, {-112, 56}, {-98, 56}, {-98, 56}}, color = {95, 95, 95}));
    connect(distance.frame_a, fixedTranslation.frame_b) annotation(
      Line(points = {{-136, 258}, {-144, 258}, {-144, 184}, {-114, 184}, {-114, 184}}, color = {95, 95, 95}));
    connect(distance.frame_b, LEO_R.LEO_end) annotation(
      Line(points = {{-116, 258}, {-96, 258}, {-96, 224}, {-96, 224}}));
    connect(fixedRotation.frame_b, sail_tension_L.frame_b) annotation(
      Line(points = {{-54, -186}, {-54, -186}, {-54, -160}, {48, -160}, {48, -144}, {48, -144}}, color = {95, 95, 95}));
    connect(sail_tension_R.frame_b, tip_R.frame_b) annotation(
      Line(points = {{56, 116}, {54, 116}, {54, 164}, {-62, 164}, {-62, 188}, {-62, 188}}, color = {95, 95, 95}));
    connect(siderwire_R.frame_a, control_Frame.BOU_corner_R) annotation(
      Line(points = {{24, 60}, {24, 50}, {8, 50}, {8, -52}}));
    connect(siderwire_R.frame_b, LEI_R.sidewire) annotation(
      Line(points = {{24, 72}, {24, 140}, {-98, 140}}, color = {95, 95, 95}));
    connect(LEI_L.sidewire, sidewire_L.frame_a) annotation(
      Line(points = {{-100, -132}, {-4, -132}, {-4, -102}, {-4, -102}}));
    connect(sidewire_L.frame_b, control_Frame.BOU_corner_L) annotation(
      Line(points = {{-4, -90}, {-4, -90}, {-4, -74}, {-32, -74}, {-32, -52}, {-32, -52}}, color = {95, 95, 95}));
    connect(average.y, sail_tension_R.Fz) annotation(
      Line(points = {{-113, -50}, {-108, -50}, {-108, 56}, {48, 56}}, color = {0, 0, 127}));
    connect(average.y, sail_tension_L.Fz) annotation(
      Line(points = {{-113, -50}, {-108, -50}, {-108, -84}, {40, -84}}, color = {0, 0, 127}));
    connect(weight_R.force[3], average.u1) annotation(
      Line(points = {{-134, 116}, {-144, 116}, {-144, -43}, {-140, -43}}, color = {0, 0, 127}));
    connect(weight_L.force[3], average.u2) annotation(
      Line(points = {{-138, -134}, {-154, -134}, {-154, -57}, {-140, -57}}, color = {0, 0, 127}));
    annotation(
    Icon(coordinateSystem(extent = {{-200, -200}, {200, 200}}, initialScale = 0.1), graphics = {Rectangle(origin = {-28, -80}, rotation = 30, fillColor = {136, 138, 133}, fillPattern = FillPattern.VerticalCylinder, extent = {{6, 98}, {-6, -126}}), Rectangle(origin = {-12, 110}, rotation = -30, fillColor = {136, 138, 133}, fillPattern = FillPattern.VerticalCylinder, extent = {{6, 98}, {-6, -126}}), Rectangle(origin = {24, 2}, rotation = 90, fillColor = {136, 138, 133}, fillPattern = FillPattern.VerticalCylinder, extent = {{6, 98}, {-6, -68}})}),
    Diagram(coordinateSystem(extent = {{-100, -200}, {100, 200}})),
    experiment(StartTime = 0, StopTime = 5, Tolerance = 1e-06, Interval = 0.01));

end Frame_Model;

  model Keel
  
    import Modelica.Constants.*;
    
    parameter Modelica.SIunits.Length NPx = 0.05795 "Set back of leading edge hinge along keel";
    parameter Modelica.SIunits.Length NPy = 0.0635 "Distance outboard of leading edge hinge from keel";
    parameter Modelica.SIunits.Length keelLength = 2.28 "Overall length of keel";
    parameter Modelica.SIunits.Length TS_dist = 2.28 "Distance from nose to tension strop/rear wire attachment point";
    parameter Modelica.SIunits.Length TOU_dist = 1.67 "Distance from nose to top of upright attachment point";
    parameter Modelica.SIunits.Length TE_dist = 1.855 "Distance from nose to trailing edge of sail";
    parameter Modelica.SIunits.Length hang_point_dist = 1.325 "Distance from nose to hang point";
    parameter Modelica.SIunits.Length dia_i = 0.05 "Internal diameter of tube";
    parameter Modelica.SIunits.Length dia_o = 0.052 "External diameter of tube";
    parameter Modelica.SIunits.ModulusOfElasticity E = 72e9 "Young's modulus of tube material";
    parameter Modelica.SIunits.SecondMomentOfArea I = (pi/4)*((dia_o/2)^4-(dia_i/2)^4) "2nd moment of area of tube";
    parameter Modelica.SIunits.RotationalSpringConstant stiffness_vert = 3*E*I/TOU_dist "Vertical stiffness of tube";
    parameter Real initial_drop = 1 "Distance below origin keel initiates at";
    
    Modelica.Mechanics.MultiBody.Parts.BodyCylinder keel_tube(angles_fixed = false, angles_start = {0, 0, 0}, density = 2400,diameter = dia_o, innerDiameter = dia_i, r = {keelLength, 0, 0}, r_0(start = {0, 0, -initial_drop})) annotation(
      Placement(visible = true, transformation(origin = {55, 5}, extent = {{-65, -65}, {65, 65}}, rotation = 0)));
    Modelica.Mechanics.MultiBody.Interfaces.Frame_a frame_a annotation(
      Placement(visible = true, transformation(origin = {-198, 2}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {-500, 2}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Parts.FixedTranslation NP_L(r = {NPx, -NPy, 0}) annotation(
      Placement(visible = true, transformation(origin = {-88, -36}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_b LE_hinge_L annotation(
      Placement(visible = true, transformation(origin = {-88, -98}, extent = {{-16, -16}, {16, 16}}, rotation = 90), iconTransformation(origin = {-428, -98}, extent = {{-16, -16}, {16, 16}}, rotation = 90)));
  Modelica.Mechanics.MultiBody.Parts.FixedTranslation NP_R(r = {NPx, NPy, 0}) annotation(
      Placement(visible = true, transformation(origin = {-88, 60}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_b LE_hinge_R annotation(
      Placement(visible = true, transformation(origin = {-88, 98}, extent = {{-16, -16}, {16, 16}}, rotation = 90), iconTransformation(origin = {-426, 94}, extent = {{-16, -16}, {16, 16}}, rotation = 90)));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_b keel_end annotation(
      Placement(visible = true, transformation(origin = {196, 10}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {498, 2}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Parts.FixedTranslation fixedTranslation1(animation = false, r = {hang_point_dist, 0, 0}) annotation(
      Placement(visible = true, transformation(origin = {32, 68}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_b hang_point annotation(
      Placement(visible = true, transformation(origin = {102, 68}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {28, 4}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Parts.FixedTranslation TOU_Translation(animation = false, r = {TOU_dist, 0, 0}) annotation(
      Placement(visible = true, transformation(origin = {34, 98}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_b TOU_anchor annotation(
      Placement(visible = true, transformation(origin = {104, 98}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {76, 4}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Parts.FixedTranslation fixedTranslation(animation = false, r = {TE_dist, 0, 0}) annotation(
      Placement(visible = true, transformation(origin = {24, -60}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_b sail_anchor annotation(
      Placement(visible = true, transformation(origin = {120, -58}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {324, 2}, extent = {{-16, -16}, {16, 16}}, rotation = -90)));
  Modelica.Mechanics.MultiBody.Parts.FixedTranslation TS_Translation(animation = false, r = {TS_dist, 0, 0}) annotation(
      Placement(visible = true, transformation(origin = {26, -82}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_b TS_anchor annotation(
      Placement(visible = true, transformation(origin = {120, -82}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {402, 2}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Joints.Revolute revolute(n = {0, 1, 0}, phi(fixed = true, start = 0), useAxisFlange = true, w(fixed = true, start = 0)) annotation(
      Placement(visible = true, transformation(origin = {-42, 2}, extent = {{-18, -18}, {18, 18}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Components.Damper vert_damp(d = stiffness_vert / 10) annotation(
      Placement(visible = true, transformation(origin = {-50, 88}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Components.Spring vert_stiff(c = stiffness_vert, phi_rel0 = 0) annotation(
      Placement(visible = true, transformation(origin = {-50, 60}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  equation
    connect(NP_L.frame_a, frame_a) annotation(
      Line(points = {{-88, -26}, {-88, 2}, {-198, 2}}, color = {95, 95, 95}));
    connect(NP_L.frame_b, LE_hinge_L) annotation(
      Line(points = {{-88, -46}, {-88, -46}, {-88, -98}, {-88, -98}}, color = {95, 95, 95}));
    connect(frame_a, NP_R.frame_a) annotation(
      Line(points = {{-198, 2}, {-88, 2}, {-88, 50}}));
    connect(NP_R.frame_b, LE_hinge_R) annotation(
      Line(points = {{-88, 70}, {-88, 70}, {-88, 98}, {-88, 98}}));
    connect(keel_tube.frame_b, keel_end) annotation(
      Line(points = {{120, 5}, {198, 5}, {198, 10}, {196, 10}}, color = {95, 95, 95}));
    connect(fixedTranslation1.frame_b, hang_point) annotation(
      Line(points = {{42, 68}, {102, 68}}, color = {95, 95, 95}));
    connect(TOU_Translation.frame_b, TOU_anchor) annotation(
      Line(points = {{44, 98}, {104, 98}}, color = {95, 95, 95}));
    connect(fixedTranslation.frame_b, sail_anchor) annotation(
      Line(points = {{34, -60}, {78, -60}, {78, -58}, {120, -58}}, color = {95, 95, 95}));
    connect(TS_Translation.frame_b, TS_anchor) annotation(
      Line(points = {{36, -82}, {120, -82}}, color = {95, 95, 95}));
    connect(frame_a, revolute.frame_a) annotation(
      Line(points = {{-198, 2}, {-60, 2}, {-60, 2}, {-60, 2}}));
    connect(revolute.frame_b, fixedTranslation.frame_a) annotation(
      Line(points = {{-24, 2}, {-18, 2}, {-18, -60}, {14, -60}, {14, -60}}, color = {95, 95, 95}));
    connect(revolute.frame_b, TS_Translation.frame_a) annotation(
      Line(points = {{-24, 2}, {-18, 2}, {-18, -80}, {16, -80}, {16, -82}}, color = {95, 95, 95}));
    connect(revolute.frame_b, fixedTranslation1.frame_a) annotation(
      Line(points = {{-24, 2}, {-18, 2}, {-18, 70}, {22, 70}, {22, 68}}));
    connect(revolute.frame_b, TOU_Translation.frame_a) annotation(
      Line(points = {{-24, 2}, {-18, 2}, {-18, 98}, {24, 98}, {24, 98}}, color = {95, 95, 95}));
    connect(revolute.frame_b, keel_tube.frame_a) annotation(
      Line(points = {{-24, 2}, {-10, 2}, {-10, 6}, {-10, 6}}, color = {95, 95, 95}));
    connect(vert_damp.flange_a, vert_stiff.flange_a) annotation(
      Line(points = {{-60, 88}, {-66, 88}, {-66, 60}, {-60, 60}}));
    connect(vert_damp.flange_b, vert_stiff.flange_b) annotation(
      Line(points = {{-40, 88}, {-40, 60}}));
    connect(vert_stiff.flange_b, revolute.axis) annotation(
      Line(points = {{-40, 60}, {-40, 34}, {-42, 34}, {-42, 20}}));
    connect(revolute.support, vert_stiff.flange_a) annotation(
      Line(points = {{-53, 20}, {-66, 20}, {-66, 60}, {-60, 60}}));
    annotation(
      Icon(coordinateSystem(extent = {{-500, -100}, {500, 100}}, initialScale = 0.1), graphics = {Rectangle(origin = {1, 1}, fillColor = {136, 138, 133}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-499, 45}, {499, -45}}), Polygon(origin = {-430, 0}, fillColor = {136, 138, 133}, fillPattern = FillPattern.Solid, points = {{4, 98}, {-60, 4}, {2, -98}, {58, 10}, {4, 98}})}),
      Diagram(coordinateSystem(extent = {{-500, -100}, {500, 100}})));
  end Keel;

  model Control_Frame
    import Modelica.SIunits.Conversions.*;
    parameter Modelica.SIunits.Length width_TOU = 0.082 "Width across Top of Upright pivot points";
    parameter Modelica.SIunits.Length width_bb = 1.345 "Width across base bar";
    parameter Modelica.SIunits.Length height = 1.44722 "Total in plane height of control frame";
    parameter Modelica.SIunits.Angle fudgeAngle = from_deg(0.1) "Annoying fudge angle to make things not quite line up!";
    parameter Modelica.SIunits.Length uprightLength = sqrt(height ^ 2 + ((width_bb - width_TOU) / 2) ^ 2);
    parameter Modelica.SIunits.Angle frameAngle = atan(height / ((width_bb - width_TOU) / 2))+fudgeAngle;
    Modelica.Mechanics.MultiBody.Interfaces.Frame_a TOU_conn annotation(
      Placement(visible = true, transformation(origin = {2, 72}, extent = {{-16, -16}, {16, 16}}, rotation = -90), iconTransformation(origin = {-2, 200}, extent = {{-16, -16}, {16, 16}}, rotation = -90)));
    Modelica.Mechanics.MultiBody.Parts.FixedTranslation TOU_R(animation = false,r = {0, width_TOU / 2, 0}, width = 0.01) annotation(
      Placement(visible = true, transformation(origin = {44, 52}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Modelica.Mechanics.MultiBody.Parts.FixedTranslation TOU_L(animation = false,r = {0, width_TOU / 2, 0}, width = 0.01) annotation(
      Placement(visible = true, transformation(origin = {-38, 52}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Modelica.Mechanics.MultiBody.Joints.Revolute TOU_joint_R(n = {1, 0, 0}, phi(fixed = true, start = -frameAngle), w(fixed = true, start = 0)) annotation(
      Placement(visible = true, transformation(origin = {64, 32}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
    Modelica.Mechanics.MultiBody.Parts.BodyCylinder upright_R(density = 2400, diameter = 0.0284, innerDiameter = 0.025, r = {0, uprightLength, 0}) annotation(
      Placement(visible = true, transformation(origin = {64, -4}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
    Modelica.Mechanics.MultiBody.Parts.BodyCylinder upright_L(density = 2400, diameter = 0.0284, innerDiameter = 0.025, r = {0, uprightLength, 0}) annotation(
      Placement(visible = true, transformation(origin = {-74, -2}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
    Modelica.Mechanics.MultiBody.Joints.Revolute TOU_joint_L(n = {1, 0, 0}, phi(fixed = true, start = -(from_deg(180) - frameAngle)), w(fixed = true, start = 0)) annotation(
      Placement(visible = true, transformation(origin = {-72, 32}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
    Modelica.Mechanics.MultiBody.Joints.Revolute BOU_L(n = {1, 0, 0}, phi(fixed = true, start = from_deg(180) - frameAngle), w(fixed = true)) annotation(
      Placement(visible = true, transformation(origin = {-74, -32}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
    Modelica.Mechanics.MultiBody.Parts.BodyCylinder bodyCylinder(density = 2400, diameter = 0.0284, innerDiameter = 0.025, r = {0, width_bb, 0}) annotation(
      Placement(visible = true, transformation(origin = {-10, -48}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Modelica.Mechanics.MultiBody.Interfaces.Frame_b BOU_corner_L annotation(
      Placement(visible = true, transformation(origin = {-122, -48}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {-194, -112}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
    Modelica.Mechanics.MultiBody.Interfaces.Frame_b BOU_corner_R annotation(
      Placement(visible = true, transformation(origin = {128, -40}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {194, -114}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Forces.SpringDamperParallel BOU_R_spring(animation = false,c = 1000000, d = 1000000) annotation(
      Placement(visible = true, transformation(origin = {64, -32}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  equation
    connect(TOU_conn, TOU_R.frame_a) annotation(
      Line(points = {{2, 72}, {2, 50}, {11, 50}, {11, 52}, {34, 52}}));
    connect(TOU_L.frame_b, TOU_R.frame_a) annotation(
      Line(points = {{-28, 52}, {34, 52}}));
    connect(TOU_R.frame_b, TOU_joint_R.frame_a) annotation(
      Line(points = {{54, 52}, {64, 52}, {64, 42}}, color = {95, 95, 95}));
    connect(TOU_joint_R.frame_b, upright_R.frame_a) annotation(
      Line(points = {{64, 22}, {64, 6}}, color = {95, 95, 95}));
    connect(TOU_joint_L.frame_a, TOU_L.frame_a) annotation(
      Line(points = {{-72, 42}, {-72, 52}, {-48, 52}}));
    connect(TOU_joint_L.frame_b, upright_L.frame_a) annotation(
      Line(points = {{-72, 22}, {-72, -21}, {-74, -21}, {-74, 8}}, color = {95, 95, 95}));
    connect(upright_L.frame_b, BOU_L.frame_a) annotation(
      Line(points = {{-74, -12}, {-74, -22}}));
    connect(BOU_L.frame_b, bodyCylinder.frame_a) annotation(
      Line(points = {{-74, -42}, {-74, -48}, {-20, -48}}, color = {95, 95, 95}));
    connect(bodyCylinder.frame_a, BOU_corner_L) annotation(
      Line(points = {{-20, -48}, {-120, -48}, {-120, -48}, {-122, -48}}, color = {95, 95, 95}));
    connect(bodyCylinder.frame_b, BOU_corner_R) annotation(
      Line(points = {{0, -48}, {128, -48}, {128, -40}, {128, -40}}, color = {95, 95, 95}));
  connect(upright_R.frame_b, BOU_R_spring.frame_a) annotation(
      Line(points = {{64, -14}, {64, -14}, {64, -22}, {64, -22}}, color = {95, 95, 95}));
  connect(BOU_R_spring.frame_b, bodyCylinder.frame_b) annotation(
      Line(points = {{64, -42}, {64, -42}, {64, -48}, {0, -48}, {0, -48}}, color = {95, 95, 95}));
    annotation(
      Icon(coordinateSystem(extent = {{-200, -200}, {200, 200}}, initialScale = 0.1), graphics = {Rectangle(origin = {-73, 66}, rotation = -25, lineColor = {52, 101, 164}, fillColor = {114, 159, 207}, fillPattern = FillPattern.VerticalCylinder, extent = {{9, 142}, {-11, -206}}), Rectangle(origin = {69, 68}, rotation = 25, lineColor = {52, 101, 164}, fillColor = {114, 159, 207}, fillPattern = FillPattern.VerticalCylinder, extent = {{9, 142}, {-11, -206}}), Rectangle(origin = {-23, -116}, rotation = 90, lineColor = {52, 101, 164}, fillColor = {114, 159, 207}, fillPattern = FillPattern.VerticalCylinder, extent = {{9, 142}, {-5, -184}})}),
      Diagram(coordinateSystem(extent = {{-200, -200}, {200, 200}})));
  end Control_Frame;

  model Leading_Edge_Inner
  
    import Modelica.Constants.*;
    
    parameter Modelica.SIunits.Length length = 4.104 "Length of inner leading edge from bolt to end of tube";
    parameter Modelica.SIunits.Length sidewire_dist = 3.234 "Distance from bolt to sidewire fixing";
    parameter Modelica.SIunits.Length xtube_x = 2.834 "Distance from bolt to cross tube junction along tube";
    parameter Modelica.SIunits.Length xtube_y = 0.03 "Distance to cross tube junction across tube";
    parameter Modelica.SIunits.Length xtube_z = 0.057 "Distance to cross tube junction above tube";
    parameter Modelica.SIunits.Length susp_point = xtube_x "Distance to supsension point along leading edge";
    parameter Modelica.SIunits.Length dia_i = 0.05 "Internal diameter of tube";
    parameter Modelica.SIunits.Length dia_o = 0.052 "External diameter of tube";
    parameter Modelica.SIunits.ModulusOfElasticity E = 72e9 "Young's modulus of tube material";
    parameter Modelica.SIunits.SecondMomentOfArea I = (pi/4)*((dia_o/2)^4-(dia_i/2)^4) "2nd moment of area of tube";
    parameter Modelica.SIunits.RotationalSpringConstant stiffness_hori = 3*E*I/(length-xtube_x) "Horizontal stiffness of tube";
    parameter Modelica.SIunits.RotationalSpringConstant stiffness_vert = 3*E*I/sidewire_dist "Vertical stiffness of tube";
    
    Modelica.Mechanics.MultiBody.Parts.BodyCylinder LEII_tube(a_0(each fixed = false),density = 2400, diameter = dia_o, innerDiameter = dia_i, r = {xtube_x, 0, 0}, r_0(each fixed = false), v_0(each fixed = false)) annotation(
      Placement(visible = true, transformation(origin = {-56, 6}, extent = {{-56, -56}, {56, 56}}, rotation = 0)));
    Modelica.Mechanics.MultiBody.Interfaces.Frame_a LE_bolt annotation(
      Placement(visible = true, transformation(origin = {-252, 8}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {-494, 0}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
    Modelica.Mechanics.MultiBody.Parts.FixedTranslation to_xtube(animation = false,r = {xtube_x, xtube_y, xtube_z}, width = 0.01) annotation(
      Placement(visible = true, transformation(origin = {-77, -87}, extent = {{-23, -23}, {23, 23}}, rotation = 0)));
    Modelica.Mechanics.MultiBody.Parts.FixedTranslation to_sidewire(animation = false,r = {sidewire_dist, 0, 0}, width = 0.01) annotation(
      Placement(visible = true, transformation(origin = {-72, 82}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Modelica.Mechanics.MultiBody.Interfaces.Frame_b LEI_end annotation(
      Placement(visible = true, transformation(origin = {268, 14}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {496, 0}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
    Modelica.Mechanics.MultiBody.Interfaces.Frame_b sidewire annotation(
      Placement(visible = true, transformation(origin = {-8, 78}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {340, 0}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
    Modelica.Mechanics.MultiBody.Interfaces.Frame_b xtube_junc annotation(
      Placement(visible = true, transformation(origin = {0, -84}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {298, 0}, extent = {{-16, -16}, {16, 16}}, rotation = 180)));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_b supension_point annotation(
      Placement(visible = true, transformation(origin = {6, -54}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {246, 0}, extent = {{-16, -16}, {16, 16}}, rotation = -90)));
  Modelica.Mechanics.MultiBody.Parts.FixedTranslation fixedTranslation(animation = false,r = {susp_point, 0, 0}, width = 0.01) annotation(
      Placement(visible = true, transformation(origin = {-71, -57}, extent = {{-23, -23}, {23, 23}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Joints.Revolute flex_LEO(n = {0, 0, 1}, phi(fixed = true, start = 0), useAxisFlange = true, w(fixed = true, start = 0))  annotation(
      Placement(visible = true, transformation(origin = {71, 13}, extent = {{-19, -19}, {19, 19}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Parts.BodyCylinder LEIO_tube( density = 2400, diameter = dia_o, innerDiameter = dia_i, r = {length -xtube_x, 0, 0}) annotation(
      Placement(visible = true, transformation(origin = {172, 14}, extent = {{-56, -56}, {56, 56}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Components.Spring spring(c = stiffness_hori, phi_rel0 = 0) annotation(
      Placement(visible = true, transformation(origin = {64, 54}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Components.Damper damper(d = stiffness_hori / 10) annotation(
      Placement(visible = true, transformation(origin = {64, 82}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Joints.Revolute revolute(n = {0, 1, 0}, phi(fixed = true, start = 0), useAxisFlange = true, w(fixed = true, start = 0)) annotation(
      Placement(visible = true, transformation(origin = {-182, 6}, extent = {{-18, -18}, {18, 18}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Components.Spring vert_stiff(c = stiffness_vert, phi_rel0 = 0) annotation(
      Placement(visible = true, transformation(origin = {-182, 52}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Components.Damper vert_damp(d = stiffness_vert / 10) annotation(
      Placement(visible = true, transformation(origin = {-182, 80}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  equation
    connect(to_xtube.frame_b, xtube_junc) annotation(
      Line(points = {{-54, -87}, {-38.5, -87}, {-38.5, -84}, {0, -84}}, color = {95, 95, 95}));
    connect(to_sidewire.frame_b, sidewire) annotation(
      Line(points = {{-62, 82}, {-8, 82}, {-8, 78}, {-8, 78}}, color = {95, 95, 95}));
    connect(fixedTranslation.frame_b, supension_point) annotation(
      Line(points = {{-48, -57}, {-38.5, -57}, {-38.5, -54}, {6, -54}}, color = {95, 95, 95}));
    connect(LEII_tube.frame_b, flex_LEO.frame_a) annotation(
      Line(points = {{0, 6}, {50, 6}, {50, 13}, {52, 13}}, color = {95, 95, 95}));
    connect(flex_LEO.frame_b, LEIO_tube.frame_a) annotation(
      Line(points = {{90, 14}, {116, 14}, {116, 14}, {116, 14}}, color = {95, 95, 95}));
    connect(LEIO_tube.frame_b, LEI_end) annotation(
      Line(points = {{228, 14}, {270, 14}, {270, 14}, {268, 14}}, color = {95, 95, 95}));
    connect(damper.flange_b, spring.flange_b) annotation(
      Line(points = {{74, 82}, {74, 54}}));
    connect(damper.flange_a, spring.flange_a) annotation(
      Line(points = {{54, 82}, {54, 54}}));
    connect(spring.flange_a, flex_LEO.support) annotation(
      Line(points = {{54, 54}, {50, 54}, {50, 32}, {60, 32}, {60, 32}}));
    connect(spring.flange_b, flex_LEO.axis) annotation(
      Line(points = {{74, 54}, {82, 54}, {82, 32}, {72, 32}, {72, 32}}));
    connect(revolute.frame_b, to_xtube.frame_a) annotation(
      Line(points = {{-164, 6}, {-136, 6}, {-136, -88}, {-100, -88}, {-100, -86}}, color = {95, 95, 95}));
    connect(revolute.frame_b, fixedTranslation.frame_a) annotation(
      Line(points = {{-164, 6}, {-136, 6}, {-136, -56}, {-94, -56}, {-94, -56}}, color = {95, 95, 95}));
    connect(revolute.frame_b, to_sidewire.frame_a) annotation(
      Line(points = {{-164, 6}, {-136, 6}, {-136, 82}, {-82, 82}, {-82, 82}}));
    connect(revolute.frame_b, LEII_tube.frame_a) annotation(
      Line(points = {{-164, 6}, {-112, 6}}));
    connect(LE_bolt, revolute.frame_a) annotation(
      Line(points = {{-252, 8}, {-200, 8}, {-200, 6}, {-200, 6}}));
    connect(vert_stiff.flange_b, revolute.axis) annotation(
      Line(points = {{-172, 52}, {-172, 24}, {-182, 24}}));
    connect(vert_stiff.flange_a, revolute.support) annotation(
      Line(points = {{-192, 52}, {-192, 24}}));
    connect(vert_damp.flange_b, vert_stiff.flange_b) annotation(
      Line(points = {{-172, 80}, {-172, 52}}));
    connect(vert_damp.flange_a, vert_stiff.flange_a) annotation(
      Line(points = {{-192, 80}, {-192, 52}}));
    annotation(
      Icon(coordinateSystem(extent = {{-500, -100}, {500, 100}}, initialScale = 0.1), graphics = {Rectangle(origin = {1, 1}, fillColor = {136, 138, 133}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-495, 33}, {495, -33}})}),
      Diagram(coordinateSystem(extent = {{-500, -100}, {500, 100}})));
  end Leading_Edge_Inner;

  model Leveller
    parameter Modelica.SIunits.Length plateWidth = 0.097 "Hole spacing in leveller plates";
    parameter Modelica.SIunits.Length lev_length = 0.364 "Length of leveller";
    parameter Modelica.SIunits.Length lev_length_TS = 0.1 "Length of levellerto tension strop attachment";
    Modelica.Mechanics.MultiBody.Parts.BodyBox leveller_body(density = 2400, height = 0.09, r = {lev_length, 0, 0}, width = 0.0145) annotation(
      Placement(visible = true, transformation(origin = {-45, 1}, extent = {{-39, -39}, {39, 39}}, rotation = 0)));
    Modelica.Mechanics.MultiBody.Parts.FixedRotation plate_R(angle = 90, n = {0, 0, 1}, r = {0, -plateWidth / 2, 0}, width = 0.01) annotation(
      Placement(visible = true, transformation(origin = {-138, -44}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
    Modelica.Mechanics.MultiBody.Parts.FixedRotation plate_L(angle = -90, n = {0, 0, 1}, r = {0, plateWidth / 2, 0}, width = 0.01) annotation(
      Placement(visible = true, transformation(origin = {-124, 78}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
    Modelica.Mechanics.MultiBody.Interfaces.Frame_b conn_lev_plate_L annotation(
      Placement(visible = true, transformation(origin = {-141, -85}, extent = {{-17, -17}, {17, 17}}, rotation = -90), iconTransformation(origin = {-68, 76}, extent = {{-16, -16}, {16, 16}}, rotation = -90)));
    Modelica.Mechanics.MultiBody.Interfaces.Frame_b conn_lev_plate_R annotation(
      Placement(visible = true, transformation(origin = {-138, 114}, extent = {{-16, -16}, {16, 16}}, rotation = -90), iconTransformation(origin = {-70, -76}, extent = {{-16, -16}, {16, 16}}, rotation = 90)));
    Modelica.Mechanics.MultiBody.Interfaces.Frame_b conn_lev_rear annotation(
      Placement(visible = true, transformation(origin = {70, 2}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {100, 2}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Parts.FixedTranslation fixedTranslation(animation = false, r = {lev_length_TS, 0, 0})  annotation(
      Placement(visible = true, transformation(origin = {-54, 80}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_b conn_lev_TS annotation(
      Placement(visible = true, transformation(origin = {44, 80}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {-30, 2}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
  equation
  connect(plate_R.frame_a, leveller_body.frame_a) annotation(
      Line(points = {{-138, -34}, {-111, -34}, {-111, 2}, {-84, 2}}));
  connect(plate_L.frame_a, leveller_body.frame_a) annotation(
      Line(points = {{-124, 68}, {-124, 2}, {-84, 2}}, color = {95, 95, 95}));
    connect(leveller_body.frame_b, conn_lev_rear) annotation(
      Line(points = {{-6, 2}, {70, 2}, {70, 2}, {70, 2}}, color = {95, 95, 95}));
  connect(plate_R.frame_b, conn_lev_plate_L) annotation(
      Line(points = {{-138, -54}, {-172, -54}, {-172, -85}, {-141, -85}}));
  connect(plate_L.frame_b, conn_lev_plate_R) annotation(
      Line(points = {{-124, 88}, {-124, 98}, {-132, 98}, {-132, 97}, {-138, 97}, {-138, 114}}, color = {95, 95, 95}));
  connect(leveller_body.frame_a, fixedTranslation.frame_a) annotation(
      Line(points = {{-84, 2}, {-102, 2}, {-102, 82}, {-64, 82}, {-64, 80}}, color = {95, 95, 95}));
  connect(fixedTranslation.frame_b, conn_lev_TS) annotation(
      Line(points = {{-44, 80}, {44, 80}, {44, 80}, {44, 80}}, color = {95, 95, 95}));
    annotation(
      Icon(coordinateSystem(extent = {{-100, -75}, {100, 75}}, initialScale = 0.1), graphics = {Polygon(points = {{-34, 72}, {-34, 72}, {-34, 72}}), Rectangle(origin = {3, 3}, fillColor = {85, 87, 83}, fillPattern = FillPattern.Solid, extent = {{-99, 11}, {99, -11}}), Polygon(origin = {-71, 0}, fillColor = {136, 138, 133}, fillPattern = FillPattern.Solid, points = {{-25, 14}, {-9, 74}, {11, 74}, {25, 14}, {25, -8}, {11, -76}, {-9, -76}, {-25, -6}, {-25, -8}, {-25, 14}})}),
      Diagram(coordinateSystem(extent = {{-500, -100}, {500, 100}})));
  end Leveller;

  model Keel_Assem
  
    parameter Modelica.SIunits.Length lev_height = 0.1 "Height of centre point of leveller above centre line of keel";  
    parameter Modelica.SIunits.Length lev_length = 0.364 "Length of leveller";
    parameter Modelica.SIunits.Length lev_length_TS = 0.1 "Length of leveller to tension strop attachment";
    parameter Modelica.SIunits.Length keelLength = 2.28 "Overall length of keel";
    parameter Modelica.SIunits.Length TSLength = 1.045 "Length of tension strop";
    parameter Modelica.SIunits.Length TS_stiff = 1e5 "Stiffness of tension strop";
    parameter Modelica.SIunits.Length NPx = 0.05795 "Set back of leading edge hinge along keel";
    parameter Modelica.SIunits.Length NPy = 0.0635 "Distance outboard of leading edge hinge from keel";
    parameter Modelica.SIunits.Length TS_dist = 2.28 "Distance from nose to tension strop/rear wire attachment point";
    parameter Modelica.SIunits.Length TOU_dist = 1.67 "Distance from nose to top of upright attachment point";
    parameter Modelica.SIunits.Length TE_dist = 1.855 "Distance from nose to trailing edge of sail";
    parameter Modelica.SIunits.Length hang_point_dist = 1.325 "Distance from nose to hang point";
  
  Frame_Model_Pack.Keel keel(NPx = NPx, NPy = NPy, TE_dist = TE_dist, TOU_dist = TOU_dist, TS_dist = TS_dist, hang_point_dist = hang_point_dist, keelLength = keelLength)  annotation(
      Placement(visible = true, transformation(origin = {4, -34}, extent = {{-50, -10}, {50, 10}}, rotation = 0)));
  Frame_Model_Pack.Leveller leveller(lev_length = lev_length, lev_length_TS = lev_length_TS)  annotation(
      Placement(visible = true, transformation(origin = {-10, 42}, extent = {{-10, -7.5}, {10, 7.5}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Joints.Prismatic lev_slider(animation = false,s(fixed = true, start = TS_dist - TSLength + lev_length - lev_length_TS), v(fixed = true))  annotation(
      Placement(visible = true, transformation(origin = {-4, -12}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Parts.FixedRotation lev_upstand(angle = -90, n = {0, 1, 0}, r = {lev_height, 0, 0})  annotation(
      Placement(visible = true, transformation(origin = {28, 6}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_a nose_conn annotation(
      Placement(visible = true, transformation(origin = {-130, -34}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {-496, -2}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Joints.Universal lev_rear_uni(animation = false,n_a = {0, 0, 1}, n_b = {0, 1, 0},phi_a(fixed = true, start = 0), phi_b(fixed = true, start = 1.5708), w_a(fixed = true, start = 0), w_b(fixed = true, start = 0))  annotation(
      Placement(visible = true, transformation(origin = {14, 42}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Forces.SpringDamperParallel tension_strop(c = TS_stiff, coilWidth = 0, d = TS_stiff / 10, diameter_a = 0.01, diameter_b = 0.02, numberOfWindings = 0, s_unstretched = TSLength, width = 0.001) annotation(
      Placement(visible = true, transformation(origin = {58, 4}, extent = {{-6, -6}, {6, 6}}, rotation = -90)));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_b keel_end_conn annotation(
      Placement(visible = true, transformation(origin = {228, -32}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {498, -4}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_b TS_anchor_conn annotation(
      Placement(visible = true, transformation(origin = {46, -62}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {442, -4}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_b sail_anchor_conn annotation(
      Placement(visible = true, transformation(origin = {18, -82}, extent = {{-16, -16}, {16, 16}}, rotation = -90), iconTransformation(origin = {376, -4}, extent = {{-16, -16}, {16, 16}}, rotation = -90)));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_b TOU_anchor_conn annotation(
      Placement(visible = true, transformation(origin = {-18, -82}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {128, -8}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_b hang_point_conn annotation(
      Placement(visible = true, transformation(origin = {-54, -74}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {58, -6}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_b lev_R_conn annotation(
      Placement(visible = true, transformation(origin = {-20, 76}, extent = {{-16, -16}, {16, 16}}, rotation = -90), iconTransformation(origin = {-162, 68}, extent = {{-16, -16}, {16, 16}}, rotation = -90)));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_b lev_L_conn annotation(
      Placement(visible = true, transformation(origin = {-34, 10}, extent = {{-16, -16}, {16, 16}}, rotation = 90), iconTransformation(origin = {-162, -82}, extent = {{-16, -16}, {16, 16}}, rotation = 90)));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_b NP_R_conn annotation(
      Placement(visible = true, transformation(origin = {-130, 10}, extent = {{-16, -16}, {16, 16}}, rotation = 90), iconTransformation(origin = {-426, 90}, extent = {{-16, -16}, {16, 16}}, rotation = 90)));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_b NP_L_conn annotation(
      Placement(visible = true, transformation(origin = {-136, -90}, extent = {{-16, -16}, {16, 16}}, rotation = 90), iconTransformation(origin = {-432, -104}, extent = {{-16, -16}, {16, 16}}, rotation = 90)));
  equation
    connect(keel.frame_a, lev_slider.frame_a) annotation(
      Line(points = {{-46, -34}, {-44, -34}, {-44, -12}, {-14, -12}}, color = {95, 95, 95}));
    connect(lev_slider.frame_b, lev_upstand.frame_b) annotation(
      Line(points = {{6, -12}, {28, -12}, {28, -4}}));
    connect(nose_conn, keel.frame_a) annotation(
      Line(points = {{-130, -34}, {-46, -34}, {-46, -34}, {-46, -34}}));
    connect(lev_upstand.frame_a, lev_rear_uni.frame_b) annotation(
      Line(points = {{28, 16}, {28, 42}, {24, 42}}));
  connect(lev_rear_uni.frame_a, leveller.conn_lev_rear) annotation(
      Line(points = {{4, 42}, {0, 42}}, color = {95, 95, 95}));
    connect(keel.TS_anchor, tension_strop.frame_b) annotation(
      Line(points = {{44, -34}, {44, -34}, {44, -12}, {56, -12}, {56, -2}, {58, -2}}, color = {95, 95, 95}));
  connect(keel.keel_end, keel_end_conn) annotation(
      Line(points = {{54, -34}, {228, -34}, {228, -32}, {228, -32}}));
  connect(keel.TS_anchor, TS_anchor_conn) annotation(
      Line(points = {{44, -34}, {46, -34}, {46, -62}, {46, -62}}, color = {95, 95, 95}));
  connect(keel.sail_anchor, sail_anchor_conn) annotation(
      Line(points = {{36, -34}, {24, -34}, {24, -82}, {18, -82}}, color = {95, 95, 95}));
  connect(TOU_anchor_conn, keel.TOU_anchor) annotation(
      Line(points = {{-18, -82}, {-18, -82}, {-18, -52}, {12, -52}, {12, -34}, {12, -34}}));
  connect(keel.hang_point, hang_point_conn) annotation(
      Line(points = {{6, -34}, {6, -48}, {-52, -48}, {-52, -74}, {-54, -74}}));
  connect(leveller.conn_lev_plate_R, lev_R_conn) annotation(
      Line(points = {{-16, 50}, {-18, 50}, {-18, 76}, {-20, 76}}, color = {95, 95, 95}));
  connect(leveller.conn_lev_plate_L, lev_L_conn) annotation(
      Line(points = {{-16, 34}, {-16, 34}, {-16, 28}, {-36, 28}, {-36, 10}, {-34, 10}}, color = {95, 95, 95}));
  connect(keel.LE_hinge_R, NP_R_conn) annotation(
      Line(points = {{-38, -24}, {-90, -24}, {-90, 8}, {-130, 8}, {-130, 10}}));
  connect(keel.LE_hinge_L, NP_L_conn) annotation(
      Line(points = {{-38, -44}, {-92, -44}, {-92, -90}, {-136, -90}, {-136, -90}}));
  connect(tension_strop.frame_a, leveller.conn_lev_TS) annotation(
      Line(points = {{58, 10}, {58, 60}, {-12, 60}, {-12, 42}, {-13, 42}}, color = {95, 95, 95}));
    annotation(
      Icon(coordinateSystem(extent = {{-500, -100}, {500, 100}}, initialScale = 0.1), graphics = {Rectangle(origin = {1, -7}, fillColor = {136, 138, 133}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-499, 45}, {499, -45}}), Polygon(origin = {-430, -8}, fillColor = {136, 138, 133}, fillPattern = FillPattern.Solid, points = {{4, 98}, {-60, 4}, {2, -98}, {58, 10}, {4, 98}}), Rectangle(origin = {-89, -3}, fillColor = {85, 87, 83}, fillPattern = FillPattern.Solid, extent = {{-99, 11}, {99, -11}}), Polygon(origin = {-163, -6}, fillColor = {136, 138, 133}, fillPattern = FillPattern.Solid, points = {{-25, 14}, {-9, 74}, {11, 74}, {25, 14}, {25, -8}, {11, -76}, {-9, -76}, {-25, -6}, {-25, -8}, {-25, 14}})}),
      Diagram(coordinateSystem(extent = {{-500, -100}, {500, 100}})));
  end Keel_Assem;

  model Leading_Edge_Outer
  
    import Modelica.Constants.*;
    
    parameter Modelica.SIunits.Length length = 1.172 "Length of inner leading edge from bolt to end of tube";
    parameter Modelica.SIunits.Length dia_i = 0.032 "Internal diameter of tube";
    parameter Modelica.SIunits.Length dia_o = 0.035 "External diameter of tube";
    parameter Modelica.SIunits.ModulusOfElasticity E = 90e9 "Young's modulus of tube material";
    parameter Modelica.SIunits.SecondMomentOfArea I = (pi/4)*((dia_o/2)^4-(dia_i/2)^4) "2nd moment of area of tube";
    parameter Modelica.SIunits.RotationalSpringConstant stiffness_hori = 3*E*I/(length) "Vertical stiffness of tube";
  
    Modelica.Mechanics.MultiBody.Interfaces.Frame_a LEO_Inner annotation(
      Placement(visible = true, transformation(origin = {-252, 8}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {-494, 0}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
    Modelica.Mechanics.MultiBody.Interfaces.Frame_b LEO_end annotation(
      Placement(visible = true, transformation(origin = {268, 14}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {496, 0}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Joints.Revolute flex_LEO(n = {0, 0, 1}, phi(fixed = true, start = 0), useAxisFlange = true, w(fixed = true, start = 0))  annotation(
      Placement(visible = true, transformation(origin = {71, 13}, extent = {{-19, -19}, {19, 19}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Parts.BodyCylinder LEIO_tube( density = 2400, diameter = dia_o, innerDiameter = dia_i, r = {length, 0, 0}) annotation(
      Placement(visible = true, transformation(origin = {172, 14}, extent = {{-56, -56}, {56, 56}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Components.Spring spring(c = stiffness_hori, phi_rel0 = 0) annotation(
      Placement(visible = true, transformation(origin = {64, 54}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Components.Damper damper(d = stiffness_hori / 10) annotation(
      Placement(visible = true, transformation(origin = {64, 82}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  equation
    connect(flex_LEO.frame_b, LEIO_tube.frame_a) annotation(
      Line(points = {{90, 14}, {116, 14}, {116, 14}, {116, 14}}, color = {95, 95, 95}));
    connect(LEIO_tube.frame_b, LEO_end) annotation(
      Line(points = {{228, 14}, {270, 14}, {270, 14}, {268, 14}}, color = {95, 95, 95}));
    connect(damper.flange_b, spring.flange_b) annotation(
      Line(points = {{74, 82}, {74, 54}}));
    connect(damper.flange_a, spring.flange_a) annotation(
      Line(points = {{54, 82}, {54, 54}}));
    connect(spring.flange_a, flex_LEO.support) annotation(
      Line(points = {{54, 54}, {50, 54}, {50, 32}, {60, 32}, {60, 32}}));
    connect(spring.flange_b, flex_LEO.axis) annotation(
      Line(points = {{74, 54}, {82, 54}, {82, 32}, {72, 32}, {72, 32}}));
  connect(LEO_Inner, flex_LEO.frame_a) annotation(
      Line(points = {{-252, 8}, {52, 8}, {52, 14}, {52, 14}}));
    annotation(
      Icon(coordinateSystem(extent = {{-500, -100}, {500, 100}}, initialScale = 0.1), graphics = {Rectangle(origin = {1, 1}, fillColor = {136, 138, 133}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-495, 33}, {495, -33}})}),
      Diagram(coordinateSystem(extent = {{-500, -100}, {500, 100}})));
  end Leading_Edge_Outer;

  model Sail_Tension_Force
  
    extends Modelica.Mechanics.Translational.Interfaces.PartialCompliant;
    
    parameter Modelica.SIunits.Length slack_length = 1 "Unstretched slack length of sail";
    parameter Real theta_start = 0.3;
    Modelica.SIunits.Angle theta(start=theta_start, min = 0.05, max=0.6);
    
    Modelica.Blocks.Interfaces.RealInput Fz annotation(
      Placement(visible = true, transformation(origin = {-108, 34}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-2, 80}, extent = {{-20, -20}, {20, 20}}, rotation = -90)));
   
   equation
  if s_rel < slack_length*0.999 then
    s_rel = slack_length * sin(theta)/theta;
    f = Fz / tan(theta);
  else
    theta = theta_start;
    f= Fz*100;
  end if;
    
  
  annotation(
      Icon(coordinateSystem(extent = {{-100, -100}, {100, 100}})),
      Diagram(coordinateSystem(extent = {{-100, -100}, {100, 100}})),
      uses(Modelica(version = "3.2.3")));
  end Sail_Tension_Force;

  model Sail_Tension "Sail tension model"
    import Modelica.Mechanics.MultiBody.Types;
    extends Modelica.Mechanics.MultiBody.Interfaces.PartialTwoFrames;
    parameter Boolean animation = true "= true, if animation shall be enabled";
    parameter Boolean showMass = true "= true, if point mass shall be visualized as sphere if animation=true and m>0";
    parameter Modelica.SIunits.Length slack_length = 1 "Unstretched slack length of sail";
    parameter Modelica.SIunits.Mass m(min = 0) = 0 "Sail mass located on the connection line between the origin of frame_a and the origin of frame_b";
    parameter Real lengthFraction(min = 0, max = 1) = 0.5 "Location of spring mass with respect to frame_a as a fraction of the distance from frame_a to frame_b (=0: at frame_a; =1: at frame_b)";
    parameter Modelica.SIunits.Distance width = 0.1 "Width of spring";
    parameter Modelica.SIunits.Distance coilWidth = width / 10 "Width of spring coil";
    parameter Real numberOfWindings = 5 "Number of spring windings";
    parameter Modelica.Mechanics.MultiBody.Types.Color color = Modelica.Mechanics.MultiBody.Types.Defaults.SpringColor "Color of spring";
    parameter Modelica.Mechanics.MultiBody.Types.SpecularCoefficient specularCoefficient = world.defaultSpecularCoefficient "Reflection of ambient light (= 0: light is completely absorbed)";
    parameter Modelica.SIunits.Diameter massDiameter = max(0, (width - 2 * coilWidth) * 0.9) "Diameter of mass point sphere";
    parameter Modelica.Mechanics.MultiBody.Types.Color massColor = Modelica.Mechanics.MultiBody.Types.Defaults.BodyColor;
    parameter Modelica.SIunits.Distance s_small = 1e-10 "Prevent zero-division if distance between frame_a and frame_b is zero";
    parameter Boolean fixedRotationAtFrame_a = false "=true, if rotation frame_a.R is fixed (to directly connect line forces)";
    parameter Boolean fixedRotationAtFrame_b = false;
    Modelica.SIunits.Position r_rel_a[3] "Position vector from origin of frame_a to origin of frame_b, resolved in frame_a";
    Real e_a[3](each final unit = "1") "Unit vector on the line connecting the origin of frame_a with the origin of frame_b resolved in frame_a (directed from frame_a to frame_b)";
    Modelica.SIunits.Force f "Line force acting on frame_a and on frame_b (positive, if acting on frame_b and directed from frame_a to frame_b)";
    Modelica.SIunits.Distance length "Distance between the origin of frame_a and the origin of frame_b";
    Modelica.SIunits.Position s "(Guarded) distance between the origin of frame_a and the origin of frame_b (>= s_small))";
    Modelica.SIunits.Position r_rel_0[3] "Position vector from frame_a to frame_b resolved in world frame";
    Real e_rel_0[3](each final unit = "1") "Unit vector in direction from frame_a to frame_b, resolved in world frame";
    Modelica.Mechanics.MultiBody.Forces.LineForceWithMass lineForce(animateLine = animation, animateMass = showMass, m = m, lengthFraction = lengthFraction, lineShapeType = "spring", lineShapeHeight = coilWidth * 2, lineShapeWidth = width, lineShapeExtra = numberOfWindings, lineShapeColor = color, specularCoefficient = specularCoefficient, massDiameter = massDiameter, massColor = massColor, s_small = s_small, fixedRotationAtFrame_a = fixedRotationAtFrame_a, fixedRotationAtFrame_b = fixedRotationAtFrame_b) annotation(
      Placement(transformation(extent = {{-20, -20}, {20, 20}})));
    Sail_Tension_Force sail_Tension_Force(s_rel(start = 0.95 * slack_length), slack_length = slack_length) annotation(
      Placement(visible = true, transformation(origin = {-2, 48}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Modelica.Blocks.Interfaces.RealInput Fz annotation(
      Placement(visible = true, transformation(origin = {-162, 100}, extent = {{-20, -20}, {20, 20}}, rotation = -90), iconTransformation(origin = {-494, 84}, extent = {{-20, -20}, {20, 20}}, rotation = -90)));
    Modelica.Mechanics.MultiBody.Forces.Damper damper(d = 100) annotation(
      Placement(visible = true, transformation(origin = {-2, -54}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  equation
  // Results
    r_rel_a = Modelica.Mechanics.MultiBody.Frames.resolve2(frame_a.R, r_rel_0);
    e_a = r_rel_a / s;
    f = sail_Tension_Force.f;
    length = lineForce.length;
    s = lineForce.s;
    r_rel_0 = lineForce.r_rel_0;
    e_rel_0 = lineForce.e_rel_0;
    connect(sail_Tension_Force.flange_b, lineForce.flange_b) annotation(
      Line(points = {{8, 48}, {26, 48}, {26, 20}, {12, 20}}, color = {0, 127, 0}));
    connect(sail_Tension_Force.flange_a, lineForce.flange_a) annotation(
      Line(points = {{-12, 48}, {-36, 48}, {-36, 18}, {-12, 18}, {-12, 20}}, color = {0, 127, 0}));
    connect(Fz, sail_Tension_Force.Fz) annotation(
      Line(points = {{-162, 100}, {-162, 72}, {-2, 72}, {-2, 56}}, color = {0, 0, 127}));
    connect(damper.frame_b, lineForce.frame_b) annotation(
      Line(points = {{8, -54}, {44, -54}, {44, 0}, {20, 0}, {20, 0}}));
    connect(lineForce.frame_a, damper.frame_a) annotation(
      Line(points = {{-20, 0}, {-44, 0}, {-44, -54}, {-12, -54}, {-12, -54}}, color = {95, 95, 95}));
  equation
    connect(lineForce.frame_a, frame_a) annotation(
      Line(points = {{-20, 0}, {-100, 0}}, color = {95, 95, 95}, thickness = 0.5));
    connect(lineForce.frame_b, frame_b) annotation(
      Line(points = {{20, 0}, {100, 0}}, color = {95, 95, 95}, thickness = 0.5));
    annotation(
      Icon(coordinateSystem(extent = {{-500, -100}, {500, 100}}, initialScale = 0.1), graphics = {Line(origin = {-1.16, 2.65}, points = {{-498.843, 1.35081}, {-420.843, 17.3508}, {-200.843, 47.3508}, {-0.843038, 61.3508}, {231.157, 49.3508}, {379.157, 29.3508}, {499.157, 3.35081}}, thickness = 1)}),
      Diagram(coordinateSystem(extent = {{-500, -100}, {500, 100}})),
      uses(Modelica(version = "3.2.3")),
      version = "",
      experiment(StartTime = 0, StopTime = 1, Tolerance = 1e-06, Interval = 0.002));
  end Sail_Tension;
  annotation(
    Icon(coordinateSystem(extent = {{-500, -100}, {500, 100}})),
    Diagram(coordinateSystem(extent = {{-500, -100}, {500, 100}})),
    uses(Modelica(version = "3.2.3")));
end Frame_Model_Pack;